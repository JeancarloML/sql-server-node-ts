import NodeServer from "./server/server";
const app = new NodeServer(process.env.PORT || 3000);
app.start();
