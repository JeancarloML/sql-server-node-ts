import express from "express";
import cors from "cors";

import sequelize from "../lib/conexion";
import productosRoutes from "../db-productos/productos.routes";
import categoriasRoutes from "../db-productos/categorias.routes";
import autoresRoutes from "../db-libros/autores.routes";
import titulosRoutes from "../db-libros/titulos.routes";
import librosRoutes from "../db-libros/libros.routes";

export default class NodeServer {
  public app: express.Application;
  public port: number | string;

  constructor(port: number | string) {
    this.port = port;
    this.app = express();
    this.app.set("port", this.port);
    this.config();
    this.dbConnection();
    this.routes();
  }

  config(): void {
    this.app.use(cors());
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: false }));
  }

  routes(): void {
    this.app.use("/api/v1/categorias", categoriasRoutes);
    this.app.use("/api/v1/productos", productosRoutes);
    this.app.use("/api/v1/autores", autoresRoutes);
    this.app.use("/api/v1/titulos", titulosRoutes);
    this.app.use("/api/v1/libros", librosRoutes);
  }

  start(): void {
    this.app.listen(this.port, () => {
      console.log(`Server running on port ${this.port}`);
    });
  }

  private async dbConnection() {
    try {
      await sequelize.authenticate();
      await sequelize.sync(/* {
        force: true,
      } */);
      console.log("Connection has been established successfully.");
    } catch (error) {
      console.error("Unable to connect to the database:", error);
    }
  }
}
