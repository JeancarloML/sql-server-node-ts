import { Sequelize } from "sequelize";
import { config } from "../config/config";

/* const sequelize = new Sequelize({
  dialect: "mysql",
  host: "localhost",
  // port: 1433, 
  port: 3306,
  username: "sa",
  password: "Super_Duper_Secret98",
  database: "db_arquitectura",
}); */

const sequelize = new Sequelize(config.DB.POSTGRES.DB_URI, {
  dialect: "postgres",
  ssl: true,
  dialectOptions: {
    ssl: {
      require: true,
      rejectUnauthorized: false,
    },
  },
});

/* export class Conexion {
  public static getConexion(): Sequelize {
    return sequelize;
  }
} */

export default sequelize;
/* /bin/bash
/opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P Yukon900 */
