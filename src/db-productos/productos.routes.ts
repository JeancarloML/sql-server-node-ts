import { Router } from "express";
import { Producto } from "./modelos";

const router = Router();

router.get("/", async (req, res) => {
  try {
    const productos = await Producto.findAll({
      include: ["categoria"],
    });
    if (productos.length === 0) throw new Error("No hay productos");
    res.status(200).json({
      ok: true,
      Message: "Productos encontrados",
      productos,
    });
  } catch (error: any) {
    return res.status(500).json({
      ok: false,
      mensaje: error.message,
    });
  }
});

router.post("/", async (req, res) => {
  try {
    const { producto, precio, idCategoria } = req.body;
    const productoExistente = await Producto.findOne({ where: { producto } });
    if (productoExistente) throw new Error("El producto ya existe");
    const nuevoProducto = await Producto.create({
      producto,
      precio,
      idCategoria,
    });
    res.status(200).json({
      ok: true,
      mensaje: "Producto creado correctamente",
      producto: nuevoProducto,
    });
  } catch (error: any) {
    return res.status(500).json({
      ok: false,
      mensaje: error.message,
    });
  }
});

router.patch("/:id", async (req, res) => {
  try {
    const producto = await Producto.update(req.body, {
      where: {
        codigo: req.params.id,
      },
    });
    console.log(producto);
    if (producto[0] === 0) throw new Error("El producto no existe");
    res.status(200).json({
      ok: true,
      mensaje: "Producto actualizado correctamente",
    });
  } catch (error: any) {
    return res.status(500).json({
      ok: false,
      mensaje: error.message,
    });
  }
});

router.delete("/:id", async (req, res) => {
  try {
    const producto = await Producto.destroy({
      where: {
        codigo: req.params.id,
      },
    });
    if (producto === 0) throw new Error("El producto no existe");
    res.status(200).json({
      ok: true,
      mensaje: "Producto eliminado correctamente",
    });
  } catch (error: any) {
    return res.status(500).json({
      ok: false,
      mensaje: error.message,
    });
  }
});

export default router;
