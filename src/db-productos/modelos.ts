import { DataTypes } from "sequelize";
import sequelize from "../lib/conexion";

export const Categoria = sequelize.define(
  "categoria",
  {
    idCategoria: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    nombreCategoria: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    timestamps: false,
  }
);

export const Producto = sequelize.define(
  "productos",
  {
    codigo: {
      allowNull: false,
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    producto: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    precio: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    idCategoria: {
      type: DataTypes.INTEGER,
      references: {
        model: "categoria",
        key: "idCategoria",
      },
      onUpdate: "CASCADE",
      onDelete: "SET NULL",
    },
  },
  {
    timestamps: false,
  }
);

Categoria.hasMany(Producto, {
  as: "productos",
  foreignKey: "idCategoria",
});

Producto.belongsTo(Categoria, {
  as: "categoria",
  foreignKey: "idCategoria",
});
