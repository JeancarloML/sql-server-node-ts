import { Router } from "express";
import { Categoria } from "./modelos";

const router = Router();

router.get("/", async (req, res) => {
  try {
    const categorias = await Categoria.findAll();
    if (categorias.length === 0) throw new Error("No hay categorias");
    res.status(200).json({
      ok: true,
      Message: "Categorias encontradas",
      productos: categorias,
    });
  } catch (error: any) {
    return res.status(500).json({
      ok: false,
      mensaje: error.message,
    });
  }
});

router.post("/", async (req, res) => {
  try {
    const categoriaExistente = await Categoria.findOne({
      where: {
        nombreCategoria: req.body.nombreCategoria,
      },
    });
    if (categoriaExistente) throw new Error("La categoria ya existe");
    const categoria = await Categoria.create(req.body);
    res.status(200).json({
      ok: true,
      mensaje: "Categoria creada correctamente",
      categoria,
    });
  } catch (error: any) {
    return res.status(500).json({
      ok: false,
      mensaje: error.message,
    });
  }
});

router.patch("/:id", async (req, res) => {
  try {
    const categoria = await Categoria.update(req.body, {
      where: {
        idCategoria: req.params.id,
      },
    });
    if (categoria[0] === 0) throw new Error("La categoria no existe");
    res.status(200).json({
      ok: true,
      mensaje: "Categoria actualizada correctamente",
    });
  } catch (error: any) {
    return res.status(500).json({
      ok: false,
      mensaje: error.message,
    });
  }
});

router.delete("/:id", async (req, res) => {
  try {
    const categoria = await Categoria.destroy({
      where: {
        idCategoria: req.params.id,
      },
    });
    if (categoria === 0) throw new Error("La categoria no existe");
    res.status(200).json({
      ok: true,
      mensaje: "Categoria eliminada correctamente",
    });
  } catch (error: any) {
    return res.status(500).json({
      ok: false,
      mensaje: error.message,
    });
  }
});

export default router;
