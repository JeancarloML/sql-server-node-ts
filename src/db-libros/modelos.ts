import { DataTypes } from "sequelize";
import sequelize from "../lib/conexion";

export const Titulos = sequelize.define(
  "titulos",
  {
    codigoTitulo: {
      allowNull: false,
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    Titulo: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
  },
  {
    timestamps: false,
  }
);

export const Autores = sequelize.define(
  "autores",
  {
    codigoAutor: {
      allowNull: false,
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    nombres: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    apellidos: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    origen: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    timestamps: false,
  }
);

export const TitulosAutores = sequelize.define(
  "titulos_autores",
  {
    codigoTitulo: {
      type: DataTypes.UUID,
      allowNull: false,

      references: {
        model: "titulos",
        key: "codigoTitulo",
      },
      onUpdate: "CASCADE",
      onDelete: "CASCADE",
    },
    codigoAutor: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: "autores",
        key: "codigoAutor",
      },
      onUpdate: "CASCADE",
      onDelete: "CASCADE",
    },
  },
  {
    timestamps: false,
  }
);

// Muchos a muchos
Titulos.belongsToMany(Autores, {
  through: TitulosAutores,
  foreignKey: "codigoTitulo",
  otherKey: "codigoAutor",
  as: "autores",
});
Autores.belongsToMany(Titulos, {
  through: TitulosAutores,
  foreignKey: "codigoAutor",
  otherKey: "codigoTitulo",
  as: "titulos",
});
