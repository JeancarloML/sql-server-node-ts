import { Router } from "express";
import { Titulos, Autores } from "./modelos";

const router = Router();

router.get("/", async (req, res) => {
  try {
    const titulos = await Titulos.findAll();
    if (titulos.length === 0) throw new Error("No hay titulos");
    res.status(200).json({
      ok: true,
      Message: "Titulos encontrados",
      titulos,
    });
  } catch (error: any) {
    return res.status(500).json({
      ok: false,
      mensaje: error.message,
    });
  }
});

router.post("/", async (req, res) => {
  try {
    const { Titulo } = req.body;
    const tituloExistente = await Titulos.findOne({
      where: {
        Titulo,
      },
    });
    if (tituloExistente) throw new Error("Titulo ya existe");
    const nuevoTitulo = await Titulos.create({
      Titulo,
    });
    res.status(200).json({
      ok: true,
      mensaje: "Titulo creado correctamente",
      titulo: nuevoTitulo,
    });
  } catch (error: any) {
    return res.status(500).json({
      ok: false,
      mensaje: error.message,
    });
  }
});

router.patch("/:id", async (req, res) => {
  try {
    const titulo = await Titulos.update(req.body, {
      where: {
        codigoTitulo: req.params.id,
      },
    });
    if (titulo[0] === 0) throw new Error("El titulo no existe");
    res.status(200).json({
      ok: true,
      mensaje: "Titulo actualizado correctamente",
    });
  } catch (error: any) {
    return res.status(500).json({
      ok: false,
      mensaje: error.message,
    });
  }
});

router.delete("/:id", async (req, res) => {
  try {
    const titulo = await Titulos.destroy({
      where: {
        codigoTitulo: req.params.id,
      },
    });
    if (titulo === 0) throw new Error("No existe el titulo");
    res.status(200).json({
      ok: true,
      mensaje: "Titulo eliminado correctamente",
    });
  } catch (error: any) {
    return res.status(500).json({
      ok: false,
      mensaje: error.message,
    });
  }
});

export default router;
