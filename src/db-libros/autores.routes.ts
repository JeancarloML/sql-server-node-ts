import { Router } from "express";
import { Autores } from "./modelos";

const router = Router();

router.get("/", async (req, res) => {
  try {
    const autores = await Autores.findAll();
    if (autores.length === 0) throw new Error("No hay autores");
    res.status(200).json({
      ok: true,
      Message: "Autores encontrados",
      autores,
    });
  } catch (error: any) {
    return res.status(500).json({
      ok: false,
      mensaje: error.message,
    });
  }
});

router.post("/", async (req, res) => {
  try {
    const { nombres, apellidos, origen } = req.body;
    const autorExistente = await Autores.findOne({
      where: {
        nombres,
        apellidos,
        origen,
      },
    });
    if (autorExistente) throw new Error("Autor ya existe");
    const nuevoAutor = await Autores.create({
      nombres,
      apellidos,
      origen,
    });
    res.status(200).json({
      ok: true,
      mensaje: "Autor creado",
      autor: nuevoAutor,
    });
  } catch (error: any) {
    return res.status(500).json({
      ok: false,
      mensaje: error.message,
    });
  }
});

router.patch("/:id", async (req, res) => {
  try {
    const autor = await Autores.update(req.body, {
      where: {
        codigoAutor: req.params.id,
      },
    });
    if (autor[0] === 0) throw new Error("No se encontró el autor");
    res.status(200).json({
      ok: true,
      mensaje: "Autor actualizado",
    });
  } catch (error: any) {
    return res.status(500).json({
      ok: false,
      mensaje: error.message,
    });
  }
});

router.delete("/:id", async (req, res) => {
  try {
    const autor = await Autores.destroy({
      where: {
        codigoAutor: req.params.id,
      },
    });
    if (autor === 0) throw new Error("No se encontró el autor");
    res.status(200).json({
      ok: true,
      mensaje: "Autor eliminado",
    });
  } catch (error: any) {
    return res.status(500).json({
      ok: false,
      mensaje: error.message,
    });
  }
});

export default router;
