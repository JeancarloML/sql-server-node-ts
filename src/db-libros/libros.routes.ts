import { Router } from "express";
import { Titulos, Autores, TitulosAutores } from "./modelos";

const router = Router();

router.get("/", async (req, res) => {
  try {
    const libros = await TitulosAutores.findAll({
      include: [
        {
          model: Titulos,
          as: "titulo",
        },
        {
          model: Autores,
          as: "autor",
        },
      ],
    });

    if (libros.length === 0) throw new Error("No hay libros");
    res.status(200).json({
      ok: true,
      Message: "Libros encontrados",
      libros,
    });
  } catch (error: any) {
    return res.status(500).json({
      ok: false,
      mensaje: error.message,
    });
  }
});

router.post("/", async (req, res) => {
  try {
    const { Titulo, codigoAutor } = req.body;
    const tituloExistente = await Titulos.findOne({
      where: {
        Titulo,
      },
    });
    if (tituloExistente) throw new Error("Titulo ya existe");
    const autorExistente = await Autores.findOne({
      where: {
        codigoAutor,
      },
    });
    if (!autorExistente) throw new Error("Autor no existe");
    const nuevoTitulo: any = await Titulos.create({
      Titulo,
    });
    const nuevoLibro = await TitulosAutores.create({
      codigoAutor,
      codigoTitulo: nuevoTitulo.codigoTitulo,
    });
    res.status(200).json({
      ok: true,
      mensaje: "Libro creado correctamente",
      libro: nuevoLibro,
    });
  } catch (error: any) {
    return res.status(500).json({
      ok: false,
      mensaje: error.message,
    });
  }
});

router.patch("/:id", async (req, res) => {
  try {
    const titulo = await Titulos.update(req.body, {
      where: {
        codigoTitulo: req.params.id,
      },
    });
    if (titulo[0] === 0) throw new Error("El titulo no existe");
    res.status(200).json({
      ok: true,
      mensaje: "Titulo actualizado correctamente",
    });
  } catch (error: any) {
    return res.status(500).json({
      ok: false,
      mensaje: error.message,
    });
  }
});

router.delete("/:id", async (req, res) => {
  try {
    const titulo = await Titulos.destroy({
      where: {
        codigoTitulo: req.params.id,
      },
    });
    if (titulo === 0) throw new Error("No existe el titulo");
    res.status(200).json({
      ok: true,
      mensaje: "Titulo eliminado correctamente",
    });
  } catch (error: any) {
    return res.status(500).json({
      ok: false,
      mensaje: error.message,
    });
  }
});

export default router;
